import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("RDS_DB_NAME", 'ebdb')
    username = os.environ.get("RDS_USERNAME", 'testuser')
    password = os.environ.get("RDS_PASSWORD", 'testpassword')
    hostname = os.environ.get("RDS_HOSTNAME", 'aa197acvsvq9vb8.crtzd8rc2g3d.us-west-2.rds.amazonaws.com')
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating FLOAT, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    #cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    entries = []
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    #cur.execute("SELECT greeting FROM message")
    #entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_to_db', methods=['POST'])
def add_to_db():
    print("Received request.")
    #print(request.form['message'])
    #msg = request.form['message']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        #import MySQLdb
        #cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    #cur.execute("INSERT INTO message (greeting) values ('" + msg + "')")
    cnx.commit()
    return hello()


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    messages = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()
        cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) values ("+ year + ",'" + title + "','" + director + "','" + actor + "','" + release_date + "'," + rating +")")
        messages = "Movie %s successfully inserted" % (title)
    except Exception as exp:
        print(exp)
        messages = "Movie %s could not be inserted - %s" % (title, str(exp))
        #import MySQLdb
        #cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cnx.commit()
    return render_template('index.html', messages=messages)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    str_exec = "UPDATE movies SET year = '" + year + "', director = '" + director + "', actor = '" + actor + "', release_date = '" + release_date + "', rating = '" + rating + "' WHERE title = '" + title + "'"
    print(str_exec)
    messages = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()
        cur.execute(str_exec)

        messages = "Movie %s successfully updated" % (title)
    except Exception as exp:
        print(exp)
        messages = "Movie %s could not be updated - %s" % (title, str(exp))
        #import MySQLdb
        #cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cnx.commit()
    return render_template('index.html', messages=messages)



@app.route('/search_movie', methods=['GET'])
def search_movie():
    search_actor = request.args['search_actor']
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    messages = ''
    entries = []
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movies WHERE actor = '" + search_actor + "'")
        entries = list(cur.fetchall())
        if entries == []:
            messages = "No movies found for actor %s" % (search_actor)
            return render_template('index.html', messages=messages)
            
    except Exception as exp:
        print(exp)
        return render_template('index.html', messages=str(exp))
        
        
        #import MySQLdb
        #cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cnx.commit()
    return render_template('index.html', entries=entries)


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    delete_title = request.form['delete_title']
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    messages = ''
    entries = []
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movies WHERE title = '" + delete_title + "'")
        entries = list(cur.fetchall())
        if entries == []:
            messages = "Movie with title %s does not exist" % (delete_title)
            return render_template('index.html', messages=messages)
        else:
            cur.execute("DELETE FROM movies WHERE title = '" + delete_title + "'")
            messages = "Movie %s successfully deleted" % (delete_title)

    except Exception as exp:
        print(exp)
        messages = "Movie %s could not be deleted - %s" % (delete_title, str(exp))
        return render_template('index.html', messages=messages)
        #import MySQLdb
        #cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cnx.commit()
    return render_template('index.html', messages=messages)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    rating_entries = []
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()
        cur.execute("SELECT year, title, director, actor, rating FROM movies WHERE rating = (SELECT MAX(rating) FROM movies)")
        rating_entries = list(cur.fetchall())

    except Exception as exp:
        print(exp)
        #import MySQLdb
        #cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cnx.commit()
    return render_template('index.html', rating_entries=rating_entries)
   

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    rating_entries = []
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()
        cur.execute("SELECT year, title, director, actor, rating FROM movies WHERE rating = (SELECT MIN(rating) FROM movies)")
        rating_entries = list(cur.fetchall())

    except Exception as exp:
        print(exp)
        #import MySQLdb
        #cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cnx.commit()
    return render_template('index.html', rating_entries=rating_entries)
   


@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')